import gradio as gr
from ultralytics import YOLO

def yolov8_predict(img):
    model = YOLO("yolov8n.pt")
    results = model(img)
    for result in results:
        markup_image = result.plot()
    return markup_image

def main():
    inputs = [
        gr.inputs.Image(type="filepath", label="Input Image"),
    ]

    outputs = gr.outputs.Image(type="numpy", label="Output Image")
    title = "Tyler McKerihan Yolov8 Gradio Test"

    demo_app = gr.Interface(
        fn=yolov8_predict,
        inputs=inputs,
        outputs=outputs,
        title=title,
        theme="huggingface"
    )

    demo_app.launch(debug=True)

if __name__ == "__main__":
    main()