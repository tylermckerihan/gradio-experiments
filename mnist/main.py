import os
import argparse
import torch
import gradio as gr
from torchvision import transforms
from PIL import Image
from model import ConvNet3L


def initialise_model(path):
    """
    Initialise and load the model ready for inference.
    """
    model = ConvNet3L()
    model.load_state_dict(torch.load(path))
    model.eval()
    return model


# Initialise the model outside a function for easy gradio use
model = initialise_model(os.path.join("data", "example.pt"))


def predict_image(image):
    """
    Accepts an numpy array of an image and predicts on it using the model.
    """
    image = transforms.ToTensor()(image).resize_(1, 28, 28).unsqueeze_(0)
    with torch.no_grad():
        output = model(image)
        pred = int(torch.argmax(output.data, dim=1).numpy()[0])
        print(f"prediction: {pred}")
        return pred


def cli(image_path: str):
    """
    Runs a provided image through the model using the cli, printing the prediction
    to console.
    """
    image = Image.open(image_path)
    predict_image(image)


def interface(model_path: str):
    """
    Responsible for launching a gradio sketchpad interface for inference testing.
    """
    inputs = [gr.Sketchpad(label="Draw Here", brush_radius=1, shape=(28, 28))]
    interface = gr.Interface(
        fn=predict_image,
        title="Tyler McKerihan Custom CNN MNIST Classifier",
        description="""A basic CNN MNIST Classifier built for experimentation 
                       with Pytorch 2 and Python 3.11""",
        inputs=inputs,
        outputs=gr.Label(label="Guess"),
    )
    interface.launch(debug=True)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--headless", nargs="?", type=bool, default=False)
    parser.add_argument(
        "--image", nargs="?", default=os.path.join("data", "example.jpg")
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    if args.headless:
        cli(image_path=args.image)
    else:
        interface(model_path=args.model)
