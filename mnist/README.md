# MNIST CNN-based Classifier
A simple custom MNIST CNN-based architecture implemented using Pytorch 2.0, Python 3.11 and interfaced by gradio.  
![Image of the interface](/mnist/.static/interface.png "Interface Preview")

## Key Technologies Used
- [Pytorch v2.0](https://pytorch.org/get-started/pytorch-2.0/): Read link for significance of 2.0 version
- [Gradio](https://gradio.app/): Easy user interface by Hugging Face
- [MLFlow](https://mlflow.org/): Experimentation tracking and MLOps

## Environment
- [Recommended] Create and activate a python virtual env using Python3.11
- pip install -r requirements.txt

## Training
train.ipynb contains logic to train a custom model.  
If necessary edit the batch_size and epochs, however I found a small number of epochs sufficient for decent performance.

## Prediction
- Place the best model from training into the data directory and save it as "example.pt"
    - Note: This was done to prevent conflating the repo size with a model file
- Predictions can be performed with either a gradio interface or headless with the cli
    - CLI: "python main.py --headless True --image {image path}"
    - Gradio: "python main.py"
