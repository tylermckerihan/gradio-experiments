# Stable Diffusion 2.1
A simple implementation of Stable Diffusion 2.1, which leverages Pytorch 2.0 for running inference on cpu, cuda and mps (macos) to generate a SD2.1 output in ~4 mins on a Macbook m1. 
![Image of the interface](/mnist/.static/interface.png "Interface Preview")

## Key Technologies Used
- [Pytorch v2.0](https://pytorch.org/get-started/pytorch-2.0/): Read link for significance of 2.0 version
- [Gradio](https://gradio.app/): Easy user interface by Hugging Face