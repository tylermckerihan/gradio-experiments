'''
TODO add a progress bar to interface
'''
import torch
import gradio as gr
from diffusers import StableDiffusionPipeline, DPMSolverMultistepScheduler


def initialise_model(model_id):
    device = 'cuda' if torch.cuda.is_available() else 'mps' if torch.backends.mps.is_available() else 'cpu'
    print(f"Using device: {device}")

    pipe = StableDiffusionPipeline.from_pretrained(model_id)
    pipe.scheduler = DPMSolverMultistepScheduler.from_config(pipe.scheduler.config)
    pipe = pipe.to(device)
    if device == 'mps': pipe.enable_attention_slicing()
    return pipe

model_id = "stabilityai/stable-diffusion-2-1"
print(f"Using model: {model_id}")
model = initialise_model(model_id)

def generate(prompt):
    return model(prompt).images[0]

def interface():
    """
    Responsible for launching a gradio sketchpad interface for inference testing.
    """
    interface = gr.Interface(
        fn=generate,
        title="Tyler McKerihan StableDiffusion 2.1 Demo",
        description="""StableDiffusion 2.1 running with Pytorch 2 and Python 3.11""",
        inputs='text',
    outputs = gr.outputs.Image(type="numpy", label="Output Image")
    )
    interface.launch(debug=True)

if __name__ == '__main__':
    interface()